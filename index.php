<?php
require_once 'Slim/Slim.php';
require_once 'Views/TwigView.php';

TwigView::$twigDirectory = dirname(__FILE__) . "/Twig";
TwigView::$twigExtensions = array(
  "Twig_Extensions_Slim"
);

$app = new Slim(array(
  'view' => 'TwigView'
));

/*$title = 'alemanac';
$nav = json_decode(file_get_contents('Content/nav.json'), true);*/

require_once 'includes/markdown.php';
require_once 'includes/routing.php';
//require_once 'includes/testimonials.php';

$app->run();