<?php
  class Project {
    public $_logo;
    public $_title;
    public $_summary;
    public $_screenshot;
    public $_description;
    
    public function __construct($logo, $title, $summary, $screenshot, $description) {
      $this->_logo        = $logo;
      $this->_title       = $title;
      $this->_summary     = $summary;
      $this->_screenshot  = $screenshot;
      $this->_description = $description;
    }
  }
?>