<?php
  $app->get('/', function() use ($app) {
    $posts = grep_content();
    
    $app->render('home.twig', array('posts' => $posts));
  });
  
  $app->get('/post/:any', function($any) use ($app) {
    $posts = grep_content();
    $show = false;
    
    foreach ($posts as $post) {
      if ($post['stub'] === $any) {
        $show = $post;
        break;
      }
    }
    
    if ($show === false) {
      $app->notFound();
      return;
    }
    
    
    $app->render('post.twig', $show);
  });
  
  $app->get('/admin', function() use ($app) {
    $posts = grep_content();
    
    $app->render('admin.twig', $posts);
  });
  
  $app->get('/new', function() use ($app) {
    if (!mkdir('Content/'.$app->request->post('stub'))) {
      echo '<p class="status">There was an error creating the post, sorry.</p>';
      return;
    }
    
    
  });
  
  function grep_content($folder = 'Content') {
    if (($dh = @opendir($folder)) === false) {
      // TODO: Here, show CMS installation stuff.
      return;
    }
    
    $content = array();
    
    while (($file = readdir($dh)) !== false) {
      if (is_dir("$folder/$file") && substr($file, 0, 1) !== '.') {
        $index = json_decode(file_get_contents("$folder/$file/index.json"), true);
        $content[] = array(
          'stub'    => $file,
          'index'   => $index,
          'timestamp' => strtotime($index['timestamp']),
          'content' => Markdown(file_get_contents("$folder/$file/content.md"))
        );
      }
    }
    
    return array_reverse(subval_sort($content, 'timestamp'));
  }
  
  function subval_sort($a, $subkey) {
    foreach($a as $k=>$v) {
      $b[$k] = strtolower($v[$subkey]);
    }
    asort($b);
    foreach($b as $key=>$val) {
      $c[] = $a[$key];
    }
    return $c;
  }